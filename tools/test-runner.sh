#!/bin/bash
#
# Bash wrapper to execute a script and report junit format report
#

set +e

# set PS4 for bash -x prompt
PS4='\e[94m$\e[0m '

# read arguments
NAME=$1
TEST_SCRIPT=$2
shift 2

# get current time (to nanosecond)
START=$(date +%s.%N)

# run the script, teeing output to log file and preserving exit codes
set -o pipefail
bash -x ${TEST_SCRIPT} 2>&1 | tee test.log
STATUS="$?"
set +o pipefail

# determine total runtime
END=$(date +%s.%N)
TIME=$(echo $(date +%s.%N) $START | awk '{printf "%.3f", $1-$2}')

# construct failure message
if [ ${STATUS} -ne 0 ]; then
  FAILED=1
  FAILURE="<failure message=\"Command failed\">$(cat test.log || echo)</failure>"
else
  FAILED=0
  FAILURE=""
fi

# write JUnit report
cat << EOF > junit-${CI_JOB_NAME_SLUG:-${NAME}}.xml
<?xml version="1.0" encoding="utf-8"?>
<testsuites>
<testsuite name="${NAME}" errors="0" failures="${FAILED}" skipped="0" tests="1" time="${TIME}" timestamp="$(date +%Y-%m-%dT%H:%M:%S%:z)" hostname="${CI_RUNNER_ID:-$(hostname -a)}">
<testcase classname="${NAME}" name="${TEST_SCRIPT}" time="${TIME}">
${FAILURE}
</testcase>
</testsuite>
</testsuites>
EOF

# propagate exit status
exit ${STATUS}
