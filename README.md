# IGWN Software Metapackages

This project contains definitions for software metapackages
(packages whose only purpose is to depend on other packages)
that simplify configuring hosts, VMs, and containers to support
various IGWN applications and services.

For documentation of the various metapackages and how to use them,
please see

<https://computing.docs.ligo.org/software/metapackages/>

## Metapackage definitions

The definitions are stored in YAML files in the
[`meta/`](https://git.ligo.org/computing/software/metapackages/-/tree/main/meta/)
directory.

## Build and test

The YAML definitions are parsed and converted into Packaging files
for a number of different (Linux) distributions by the 
[`generate.py`](https://git.ligo.org/computing/software/metapackages/-/blob/main/tools/generate.py)
script.

These Packaging files are then built into binary packages and tested in the
[CI/CD pipeline](https://git.ligo.org/computing/software/metapackages/-/pipelines).

## Contributing

All interactions related to this project should follow the
[LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).

For more details on contributing to this project, see `CONTRIBUTING.md`.
