<!-- Please read this!

Thank you for your interest in reporting an issue.
Before opening a new ticket, make sure to search the list of older issues
to make sure this issue hasn't been reported before, and to see whether a
solution has already been presented.

Please select the relevant report template by selecting from the
dropdown menu found under the 'Description' heading above:

- "New": You would like to request a new metapackage
- "Add": You would like to add a package to an existing metapackage
- "Bug": You would like to report a problem with an existing metapackage

Posts/comments should be formatted using GitLab Flavoured Markdown,
for instructions on the formatting, please see

https://docs.gitlab.com/ee/user/markdown.html

If you aren't sure what to do at any point, please contact the IGWN Computing team on chat at:

https://chat.ligo.org/ligo/computing-help


Thank you
The IGWN Computing Team
-->
