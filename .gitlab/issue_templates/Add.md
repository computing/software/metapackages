<!--
Thank you for your interest in adding a package to an
IGWN software metapackage.

Please complete the following template as best you can to allow the
IGWN Computing Team to evaluate and act on your request.

Blocks starting with '<--' and ending with '- ->' are comments only
visible while editing the markdown, feel free to delete them if
they get in the way.

If you aren't sure what to do at any point, please contact the IGWN Computing team on chat at:

https://chat.ligo.org/ligo/computing-help
-->

#### Metapackage update request

**Metapackage name(s)**:

<!-- please list the target metapackages here -->
- `igwn-<FIXME>`

**Package name(s)**:

<!-- please list the relevant package(s) here -->
- `<FIXME>`

**Platforms**:

<!-- please select one (or more) of the following platforms -->
- [ ] Conda (conda-forge)
- [ ] Debian 11
- [ ] Debian 12
- [ ] Rocky Linux 8
- [ ] Rocky Linux 9

<!-- please edit the labels here to match -->
/label ~conda
/label ~debian
/label ~rhel

**Purpose**:

<!-- Describe the use-case for this metapackage here.
     If this is described in another ticket, please add a link here. -->

**Notes**:

<!-- Please add any other notes or comments here. -->
