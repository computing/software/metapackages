<!--

Thank you for reporting a bug in an IGWN software metapackage

Please complete the following template as best you can to allow the
IGWN Computing Team to act on your report as quickly as possible.

If you aren't sure what to do at any point, please contact the IGWN Computing team on chat at:

https://chat.ligo.org/ligo/computing-help
-->

#### Problem summary

<!--
please (briefly) describe the issue,
include any bug reports/tracebacks you get from the commands you run
-->

#### To reproduce

<!--
please include enough code/context to reproduce the
issue on the same system (if possible)
or an empty container for the same platform
-->

#### What should happen?

<!-- please describe what you think should happen -->

#### System information

<!--
please include any system information that may be relevant,
including the output of the commands listed below
-->

```console
$ hostname -f
<INSERT hostname -f OUTPUT HERE>
$ uname -a
<INSERT uname -a OUTPUT HERE>
```
