# base
mkdocs

# theme
mkdocs-material-igwn

# extensions
mkdocs-include-markdown-plugin ~=3.1.4
mkdocs-redirects >=1.0.0
pymdown-extensions

# scripts
jinja2
python-dateutil
ruamel.yaml
